//
//  AlbumTableViewCell.swift
//  iOSTest
//
//  Created by Marco on 09/06/21.
//

import UIKit

class AlbumTableViewCell: UITableViewCell {
    
    @IBOutlet var album_name: UILabel!
    @IBOutlet var artist_name: UILabel!
    @IBOutlet var album_cover: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    public func configure(album: Album){
        self.album_cover.loadImageFromUrl(url: album.artworkUrl100)
        self.album_name.text = album.collectionName
        self.artist_name.text = album.artistName
    }

}
