//
//  WebService.swift
//  iOSTest
//
//  Created by Marco on 09/06/21.
//

import Foundation

class WebService{
    
    enum webServiceError {
        case parseError
        case requestError
        case urlError
    }
    
    typealias albumsRequestHandler = ([Album]?, webServiceError?) -> Void
    
    //singleton instance
    static let shared: WebService = {
        let instance = WebService()
        return instance
    }()
    
    
    /*
     * Gets all the albums for a certain artist and decodes the json response from the API
     */
    public func getAlbums(completion: @escaping albumsRequestHandler){
        guard let url = URL(string: "https://itunes.apple.com/search?term=dream%20theater&entity=album&limit=20") else {
            completion(nil, webServiceError.urlError)
            return
        }
        
        URLSession.shared.dataTask(with: url) { (data, _, error) in
            if error != nil {
                completion(nil, webServiceError.requestError)
            }
            
            guard let data = data else {
                completion(nil, webServiceError.parseError)
                return
            }
            
            do {
                let albumResponse = try JSONDecoder().decode(AlbumResponse.self, from: data)
                DispatchQueue.main.async {
                    completion(albumResponse.results, nil)
                }
            } catch let decodableError {
                print(decodableError)
                completion(nil, webServiceError.parseError)
            }
        }.resume()
    }
}
