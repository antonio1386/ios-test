//
//  UIButton.swift
//  iOSTest
//
//  Created by Marco on 09/06/21.
//

import Foundation
import UIKit

extension UIButton{
    public func makeWithRoundCorners(){
        self.layer.cornerRadius = 5
        self.clipsToBounds = true
    }
}
