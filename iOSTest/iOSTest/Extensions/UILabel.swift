//
//  UILabel.swift
//  iOSTest
//
//  Created by Marco on 09/06/21.
//

import Foundation
import UIKit

extension UILabel{
    
    public func printDateWithFormat(date: String){
        let dateFormatter = DateFormatter()
        let dayFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        dayFormatter.dateFormat = "EEEE MMM d, yyyy"
        
        guard let date_obj = dateFormatter.date(from: date) else{
            return
        }
        let display_day = dayFormatter.string(from: date_obj)
        self.text = display_day
    }
}
