//
//  Album.swift
//  iOSTest
//
//  Created by Marco on 09/06/21.
//

import Foundation

struct AlbumResponse: Decodable {
    let resultCount: Int
    var results = [Album]()
}

struct Album: Decodable{
    let artistName: String
    let collectionName: String
    let artworkUrl100: String
    let trackCount: Int
    let releaseDate: String
    let collectionPrice: Float?
    let currency: String
    let collectionViewUrl: String
}
