//
//  AlbumViewController.swift
//  iOSTest
//
//  Created by Marco on 09/06/21.
//

import UIKit

class AlbumViewController: UIViewController {
    
    @IBOutlet var artwork: UIImageView!
    @IBOutlet var albumName: UILabel!
    @IBOutlet var artistName: UILabel!
    @IBOutlet var numberOfSongs: UILabel!
    @IBOutlet var releaseDate: UILabel!
    @IBOutlet var price: UILabel!
    
    @IBOutlet var viewButton: UIButton!
    
    var album: Album!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
    }
    
    public func getAlbum(album: Album){
        self.album = album
    }
    
    private func configureView(){
        viewButton.makeWithRoundCorners()
        
        //loading image from url via extension
        artwork.loadImageFromUrl(url: album.artworkUrl100)
        
        //setting of label texts
        albumName.text = album.collectionName
        artistName.text = album.artistName
        numberOfSongs.text = "# of Songs: "+String(album.trackCount)
        if album.collectionPrice == nil {
            price.isHidden = true
        }else{
            let price_string = String(album.collectionPrice!)
            price.text = "$ "+price_string+" "+album.currency
        }
        
        //formating the date via extension
        releaseDate.printDateWithFormat(date: album.releaseDate)
    }
    

    //MARK: Actions
    @IBAction private func closeModal(_ sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction private func viewInAppleMusic(){
        guard let url = URL(string: album.collectionViewUrl) else { return }
        UIApplication.shared.open(url, completionHandler: nil)
    }

}
