//
//  AlbumsTableViewController.swift
//  iOSTest
//
//  Created by Marco on 09/06/21.
//

import UIKit

class AlbumsTableViewController: UITableViewController, UISearchBarDelegate {
    
    var albums = [Album]()
    var filtered = [Album]()
    
    @IBOutlet var searchBar: UISearchBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureView()
        getData()
    }
    
    private func configureView(){
        self.title = "Albums"
        
        //delegate
        searchBar.delegate = self
    }
    
    private func getData(){
        WebService().getAlbums { (albums, error) in
            if error != nil {
                print("There was an error processing the request")
            }else{
                DispatchQueue.main.async {
                    self.albums = albums ?? []
                    self.filtered = self.albums
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    //MARK: Segues
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showAlbum" {
            let vc = segue.destination as! AlbumViewController
            let album = filtered[tableView.indexPathForSelectedRow!.row]
            vc.getAlbum(album: album)
        }
    }
    
    //MARK: SearchBar
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty{
            filtered = albums
        }else{
            filtered.removeAll()
            for album in albums{
                if album.collectionName.lowercased().contains(searchText.lowercased()){
                    filtered.append(album)
                }
            }
        }
        tableView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return filtered.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 95
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "albumCell", for: indexPath) as! AlbumTableViewCell
        let album = filtered[indexPath.row]
        cell.configure(album: album)
        return cell
    }
    
    //remove the keyboard for searching when the users start to scroll
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.searchBar.endEditing(true)
    }
}
